#include <catch2/catch.hpp>

#include <rkcl/core.h>
#include <yaml-cpp/yaml.h>

TEST_CASE("joint data resize")
{
    rkcl::JointData joint;

    auto check =
        [&joint](size_t size) {
            REQUIRE(joint.position().size() == size);
            REQUIRE(joint.velocity().size() == size);
            REQUIRE(joint.acceleration().size() == size);
            REQUIRE(joint.force().size() == size);
        };

    check(0);

    joint.resize(6);
    check(6);

    joint.resize(3);
    check(3);
}

TEST_CASE("joint data configuration")
{
    rkcl::JointData joint;
    YAML::Node config;

    REQUIRE_FALSE(joint.configure(config));

    SECTION("empty joint configuration")
    {
        config["position"] = std::vector<double>();
        REQUIRE(joint.configure(config));

        config["velocity"] = std::vector<double>();
        REQUIRE(joint.configure(config));

        config["acceleration"] = std::vector<double>();
        REQUIRE(joint.configure(config));

        config["force"] = std::vector<double>();
        REQUIRE(joint.configure(config));

        REQUIRE(joint.configurePosition(config["position"]));
        REQUIRE(joint.configureVelocity(config["velocity"]));
        REQUIRE(joint.configureAcceleration(config["acceleration"]));
        REQUIRE(joint.configureForce(config["force"]));
    }

    SECTION("wrong joint configuration size")
    {
        config["position"] = std::vector<double>(6, 0.5);
        REQUIRE_FALSE(joint.configurePosition(config["position"]));

        config["velocity"] = std::vector<double>(4, 0.5);
        REQUIRE_FALSE(joint.configureVelocity(config["velocity"]));

        config["acceleration"] = std::vector<double>(2, 0.5);
        REQUIRE_FALSE(joint.configureAcceleration(config["acceleration"]));

        config["force"] = std::vector<double>(7, 0.5);
        REQUIRE_FALSE(joint.configureForce(config["force"]));

        REQUIRE_FALSE(joint.configure(config));
    }

    SECTION("proper joint configuration")
    {
        joint.resize(3);

        auto joint_positions = std::vector<double>({0.1, 0.2, 0.3});
        auto joint_velocities = std::vector<double>({1., 2., 3.});
        auto joint_accelerations = std::vector<double>({10., 20., 30.});
        auto joint_forces = std::vector<double>({100., 200., 300.});

        config["position"] = joint_positions;
        config["velocity"] = joint_velocities;
        config["acceleration"] = joint_accelerations;
        config["force"] = joint_forces;

        REQUIRE(joint.configurePosition(config["position"]));
        REQUIRE(joint.configureVelocity(config["velocity"]));
        REQUIRE(joint.configureAcceleration(config["acceleration"]));
        REQUIRE(joint.configureForce(config["force"]));

        REQUIRE(joint.position() == Eigen::Vector3d(joint_positions.data()));
        REQUIRE(joint.velocity() == Eigen::Vector3d(joint_velocities.data()));
        REQUIRE(joint.acceleration() == Eigen::Vector3d(joint_accelerations.data()));
        REQUIRE(joint.force() == Eigen::Vector3d(joint_forces.data()));

        REQUIRE(joint.configure(config));

        REQUIRE(joint.position() == Eigen::Vector3d(joint_positions.data()));
        REQUIRE(joint.velocity() == Eigen::Vector3d(joint_velocities.data()));
        REQUIRE(joint.acceleration() == Eigen::Vector3d(joint_accelerations.data()));
        REQUIRE(joint.force() == Eigen::Vector3d(joint_forces.data()));
    }
}
