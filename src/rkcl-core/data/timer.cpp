/**
 * @file timer.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a timer (compatible with simulation)
 * @date 30-01-2020
 * License: CeCILL
 */

#include <rkcl/data/timer.h>
#include <iostream>

using namespace rkcl;

double Timer::real_time_factor_ = 1;

Timer::Timer() = default;

void Timer::sleepFor(const std::chrono::microseconds& sleep_duration)
{
    sleepFor(std::chrono::high_resolution_clock::now(), sleep_duration);
}

void Timer::sleepFor(const std::chrono::high_resolution_clock::time_point& reference_time, const std::chrono::microseconds& sleep_duration)
{
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>((reference_time + sleep_duration * (1 / real_time_factor_)) - std::chrono::high_resolution_clock::now());
    if (duration.count() > 0)
    {
        std::this_thread::sleep_for(duration);
    }
    else
    {
        std::cout << "Timer::sleepFor WARNING : No time to sleep ! \n";
    }
}