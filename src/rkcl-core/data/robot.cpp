/**
 * @file robot.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a generic robot
 * @date 30-01-2020
 * License: CeCILL
 */
#include <rkcl/data/robot.h>
#include <yaml-cpp/yaml.h>

#include <numeric>

#include <iostream>

using namespace rkcl;

Robot::Robot(const YAML::Node& configuration)
{
    configure(configuration);
}

void Robot::configure(const YAML::Node& configuration)
{
    if (configuration)
    {
        const auto& name = configuration["name"];
        if (name)
        {
            this->name() = name.as<std::string>();
        }

        configureObservationPoints(configuration["observation_points"]);
        configureControlPoints(configuration["control_points"]);
        configureJointGroups(configuration["joint_groups"]);
    }
}

void Robot::configureObservationPoints(const YAML::Node& observation_points_config)
{
    if (observation_points_config)
    {
        for (const auto& observation_point : observation_points_config)
        {
            ObservationPointPtr op;
            auto observation_point_name = observation_point["name"].as<std::string>();
            auto found_observation_point = std::find(
                observation_points_.begin(),
                observation_points_.end(),
                observation_point_name);

            if (found_observation_point == observation_points_.end())
            {
                op = std::make_shared<ObservationPoint>();
                add(op);
            }
            else
            {
                op = *found_observation_point;
            }

            op->configure(observation_point);
        }
    }
}

void Robot::configureControlPoints(const YAML::Node& control_points_config)
{
    if (control_points_config)
    {
        for (const auto& control_point : control_points_config)
        {
            ControlPointPtr cp;
            auto cp_name = control_point["name"].as<std::string>();
            auto found_control_point = std::find(
                control_points_.begin(),
                control_points_.end(),
                cp_name);

            if (found_control_point == control_points_.end())
            {
                cp = std::make_shared<ControlPoint>();
                add(cp);
            }
            else
            {
                cp = *found_control_point;
            }
            cp->configure(control_point);
        }
    }
}

void Robot::configureJointGroups(const YAML::Node& joint_groups_config)
{
    if (joint_groups_config)
    {
        for (const auto& joint_group : joint_groups_config)
        {
            JointGroupPtr jg;
            auto jg_name = joint_group["name"].as<std::string>();
            auto found_joint_group = std::find(
                joint_groups_.begin(),
                joint_groups_.end(),
                jg_name);

            if (found_joint_group == joint_groups_.end())
            {
                jg = std::make_shared<JointGroup>();
                add(jg);
                jg->configure(joint_group);
            }
            else
            {
                jg = *found_joint_group;
                jg->reConfigure(joint_group);
            }
        }
    }
}

size_t Robot::jointCount() const
{
    return std::accumulate(
        joint_groups_.begin(),
        joint_groups_.end(),
        0,
        [](auto a, auto b) { return a + b->jointCount(); });
}

void Robot::add(const ControlPointPtr& control_point)
{
    control_points_.push_back(control_point);
}

void Robot::add(const ObservationPointPtr& observation_point)
{
    observation_points_.push_back(observation_point);
}

void Robot::add(const JointGroupPtr& joint_group)
{
    joint_groups_.push_back(joint_group);
}

// TODO Put this in a specific class
// const Eigen::Matrix<double, 3, 1>& Robot::getJointVelocityCommandNorms()
// {
//     return joint_velocity_norms_;
// }

// void Robot::computeJointVelocityCommandNorms()
// {
//     Eigen::VectorXd joint_velocity_command(jointCount());
//     size_t index = 0;
//     for (const auto& joint_group : joint_groups)
//     {
//         joint_velocity_command.segment(index, joint_group->jointCount()) = joint_group->command.velocity;
//         index += joint_group->jointCount();
//     }

//     joint_velocity_norms_.setZero();

//     for (size_t i = 0; i < joint_velocity_command.size(); ++i)
//         if (abs(joint_velocity_command[i]) > 1e-3)
//             joint_velocity_norms_[0]++;

//     joint_velocity_norms_[1] = joint_velocity_command.lpNorm<1>();
//     joint_velocity_norms_[2] = joint_velocity_command.squaredNorm();
// }

void Robot::publishJointCommand()
{
    for (auto& joint_group : joint_groups_)
    {
        joint_group->publishJointCommand();
    }
}

void Robot::publishCartesianConstraints()
{
    for (auto& joint_group : joint_groups_)
    {
        joint_group->publishCartesianConstraints();
    }
}

void Robot::resetInternalConstraints()
{
    for (auto& joint_group : joint_groups_)
    {
        joint_group->resetInternalConstraints();
    }
}

void Robot::updateControlPointsEnabled()
{
    for (auto cp_idx = 0u; cp_idx < controlPointCount(); ++cp_idx)
    {
        const auto& cp = controlPoints()[cp_idx];
        cp->_isControlledByJoints() = false;
        for (const auto& jg_control_name : cp->kinematics().jointGroupControlNames())
        {
            if (jointGroup(jg_control_name)->controlSpace() == JointGroup::ControlSpace::TaskSpace)
            {
                cp->_isControlledByJoints() = true;
                break;
            }
        }
    }
}

void Robot::sortJointGroupsByPriority()
{
    std::sort(joint_groups_.begin(), joint_groups_.end());
}
void Robot::sortControlPointsByPriority()
{
    std::sort(control_points_.begin(), control_points_.end());
}

JointGroupPtr Robot::jointGroup(const std::string& joint_group_name)
{
    for (size_t i = 0; i < jointGroupCount(); ++i)
    {
        if (jointGroup(i)->name() == joint_group_name)
        {
            return joint_groups_[i];
        }
    }

    return JointGroupPtr();
}

ObservationPointPtr Robot::observationPoint(const std::string& observation_point_name)
{
    auto found_observation_point = std::find(observation_points_.begin(), observation_points_.end(), observation_point_name);
    if (found_observation_point != observation_points_.end())
    {
        return *found_observation_point;
    }

    auto found_control_point = std::find(control_points_.begin(), control_points_.end(), observation_point_name);
    if (found_control_point != control_points_.end())
    {
        return *found_control_point;
    }

    return ObservationPointPtr();
}

ControlPointPtr Robot::controlPoint(const std::string& control_point_name)
{
    auto found_control_point = std::find(control_points_.begin(), control_points_.end(), control_point_name);
    if (found_control_point != control_points_.end())
    {
        return *found_control_point;
    }

    return ControlPointPtr();
}

void Robot::removeControlPoint(const std::string& control_point_name)
{
    auto found_control_point = std::find(control_points_.begin(), control_points_.end(), control_point_name);
    if (found_control_point != control_points_.end())
        if (found_control_point->use_count() > 2)
            std::cerr << "Impossible to remove Control point '" << control_point_name << "' as it is still used by another process" << std::endl;
        else
            control_points_.erase(found_control_point);
    else
        std::cerr << "Control point '" << control_point_name << "' not found. Impossible to remove" << std::endl;
}

void Robot::removeObservationPoint(const std::string& observation_point_name)
{
    auto found_observation_point = std::find(observation_points_.begin(), observation_points_.end(), observation_point_name);
    if (found_observation_point != observation_points_.end())

        if (found_observation_point->use_count() > 2)
            std::cerr << "Impossible to remove Observation point '" << observation_point_name << "' as it is used by another process" << std::endl;
        else
            observation_points_.erase(found_observation_point);
    else
        std::cerr << "Observation point '" << observation_point_name << "' not found. Impossible to remove" << std::endl;
}

void Robot::estimateControlPointsStateTwistAndAcceleration(const double& control_time_step)
{
    for (size_t cp = 0; cp < controlPointCount(); ++cp)
    {
        auto prev_twist = controlPoint(cp)->state().twist();
        estimateControlPointStateTwist(cp);
        estimateControlPointStateAcceleration(cp, prev_twist, control_time_step);
    }
}

void Robot::estimateControlPointStateTwist(size_t index)
{
    controlPoint(index)->_state().twist().setZero();
    for (auto& joint_group : jointGroups())
        controlPoint(index)->_state().twist() += controlPoint(index)->kinematics().jointGroupJacobian().find(joint_group->name())->second * joint_group->command().velocity();
}

void Robot::estimateControlPointStateAcceleration(size_t index, const Eigen::Matrix<double, 6, 1>& prev_twist, const double& control_time_step)
{
    controlPoint(index)->_state().acceleration() = (controlPoint(index)->state().twist() - prev_twist) / control_time_step;
}