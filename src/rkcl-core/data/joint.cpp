/**
 * @file joints.cpp
 * @author Sonny Tarbouriech (LIRMM), Benjamin Navarro (LIRMM)
 * @brief Define a joint group
 * @date 30-01-2020
 * License: CeCILL
 */

#include <rkcl/data/joint.h>
#include <yaml-cpp/yaml.h>
#include <vector>
#include <iostream>
#include <cassert>

using namespace rkcl;

bool JointData::configure(const YAML::Node& configuration)
{
    bool ok = false;
    ok |= configurePosition(configuration["position"]);
    ok |= configureVelocity(configuration["velocity"]);
    ok |= configureAcceleration(configuration["acceleration"]);
    ok |= configureForce(configuration["force"]);
    return ok;
}

bool JointData::configurePosition(const YAML::Node& position)
{
    if (position)
    {
        auto positions = position.as<std::vector<double>>();
        if (static_cast<Eigen::Index>(positions.size()) != this->position().size())
        {
            std::cerr << "JointData::configure: invalid joint count (" << positions.size() << " != " << this->position().size() << ")" << std::endl;
            return false;
        }
        std::copy_n(positions.begin(), positions.size(), this->position().data());
        return true;
    }
    return false;
}

bool JointData::configureVelocity(const YAML::Node& velocity)
{
    if (velocity)
    {
        auto velocities = velocity.as<std::vector<double>>();
        if (static_cast<Eigen::Index>(velocities.size()) != this->velocity().size())
        {
            std::cerr << "JointData::configure: invalid joint count (" << velocities.size() << " != " << this->velocity().size() << ")" << std::endl;
            return false;
        }
        std::copy_n(velocities.begin(), velocities.size(), this->velocity().data());
        return true;
    }
    return false;
}

bool JointData::configureAcceleration(const YAML::Node& acceleration)
{
    if (acceleration)
    {
        auto accelerations = acceleration.as<std::vector<double>>();
        if (static_cast<Eigen::Index>(accelerations.size()) != this->acceleration().size())
        {
            std::cerr << "JointData::configure: invalid joint count (" << accelerations.size() << " != " << this->acceleration().size() << ")" << std::endl;
            return false;
        }
        std::copy_n(accelerations.begin(), accelerations.size(), this->acceleration().data());
        return true;
    }
    return false;
}

bool JointData::configureForce(const YAML::Node& force)
{
    if (force)
    {
        auto forces = force.as<std::vector<double>>();
        if (static_cast<Eigen::Index>(forces.size()) != this->force().size())
        {
            std::cerr << "JointData::configure: invalid joint count (" << forces.size() << " != " << this->force().size() << ")" << std::endl;
            return false;
        }
        std::copy_n(forces.begin(), forces.size(), this->force().data());
        return true;
    }
    return false;
}

void JointData::resize(size_t joint_count)
{
    position_.resize(joint_count);
    position_.setZero();
    velocity_.resize(joint_count);
    velocity_.setZero();
    acceleration_.resize(joint_count);
    acceleration_.setZero();
    force_.resize(joint_count);
    force_.setZero();
}

bool JointLimits::configure(const YAML::Node& configuration)
{
    bool ok = false;
    ok |= configureMinPosition(configuration["min_position"]);
    ok |= configureMaxPosition(configuration["max_position"]);
    ok |= configureMaxVelocity(configuration["max_velocity"]);
    ok |= configureMaxAcceleration(configuration["max_acceleration"]);
    return ok;
}

bool JointLimits::configureMinPosition(const YAML::Node& min_position)
{
    if (min_position)
    {
        auto min_positions = min_position.as<std::vector<double>>();
        if (static_cast<Eigen::Index>(min_positions.size()) != this->minPosition().size())
        {
            std::cerr << "JointLimits::configure: invalid joint count (" << min_positions.size() << " != " << this->minPosition().size() << ")" << std::endl;
            return false;
        }
        std::copy_n(min_positions.begin(), min_positions.size(), this->minPosition().data());
        min_position_configured_ = true;
        return true;
    }
    return false;
}

bool JointLimits::configureMaxPosition(const YAML::Node& max_position)
{
    if (max_position)
    {
        auto max_positions = max_position.as<std::vector<double>>();
        if (static_cast<Eigen::Index>(max_positions.size()) != this->maxPosition().size())
        {
            std::cerr << "JointLimits::configure: invalid joint count (" << max_positions.size() << " != " << this->maxPosition().size() << ")" << std::endl;
            return false;
        }
        std::copy_n(max_positions.begin(), max_positions.size(), this->maxPosition().data());
        max_position_configured_ = true;
        return true;
    }
    return false;
}

bool JointLimits::configureMaxVelocity(const YAML::Node& max_velocity)
{
    if (max_velocity)
    {
        auto max_velocities = max_velocity.as<std::vector<double>>();
        if (static_cast<Eigen::Index>(max_velocities.size()) != this->maxVelocity()->size())
        {
            std::cerr << "JointLimits::configure: invalid joint count (" << max_velocities.size() << " != " << this->maxVelocity()->size() << ")" << std::endl;
            return false;
        }

        Eigen::VectorXd max_vel(max_velocities.size());
        std::copy_n(max_velocities.begin(), max_velocities.size(), max_vel.data());
        this->maxVelocity() = max_vel;
        max_velocity_configured_ = true;
        return true;
    }
    return false;
}

bool JointLimits::configureMaxAcceleration(const YAML::Node& max_acceleration)
{
    if (max_acceleration)
    {
        auto max_accelerations = max_acceleration.as<std::vector<double>>();
        if (static_cast<Eigen::Index>(max_accelerations.size()) != this->maxAcceleration()->size())
        {
            std::cerr << "JointLimits::configure: invalid joint count (" << max_accelerations.size() << " != " << this->maxAcceleration()->size() << ")" << std::endl;
            return false;
        }
        Eigen::VectorXd max_acc(max_accelerations.size());
        std::copy_n(max_accelerations.begin(), max_accelerations.size(), max_acc.data());
        this->maxAcceleration() = max_acc;
        max_acceleration_configured_ = true;
        return true;
    }
    return false;
}

void JointLimits::resize(size_t joint_count)
{
    min_position_.resize(joint_count);
    min_position_.setConstant(-std::numeric_limits<double>::infinity());
    max_position_.resize(joint_count);
    max_position_.setConstant(std::numeric_limits<double>::infinity());
    max_velocity_.resize(joint_count);
    max_velocity_.setConstant(std::numeric_limits<double>::infinity());
    max_acceleration_.resize(joint_count);
    max_acceleration_.setConstant(std::numeric_limits<double>::infinity());
}
