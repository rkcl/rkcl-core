/**
 * @file dummy_driver.cpp
 * @author Benjamin Navarro (LIRMM)
 * @brief Dummy driver that mirrors the commands to the state
 * @date 30-01-2020
 * License: CeCILL
 */

#include <rkcl/drivers/dummy_joints_driver.h>

#include <yaml-cpp/yaml.h>
#include <thread>
#include <chrono>

#include <iostream>
#include <utility>

namespace rkcl
{

bool DummyJointsDriver::registered_in_factory_ = DriverFactory::add<DummyJointsDriver>("dummy");

DummyJointsDriver::DummyJointsDriver(
    JointGroupPtr joint_group)
    : JointsDriver(std::move(joint_group))
{
}

DummyJointsDriver::DummyJointsDriver(
    Robot& robot,
    const YAML::Node& configuration)
{
    std::string joint_group;
    try
    {
        joint_group = configuration["joint_group"].as<std::string>();
    }
    catch (...)
    {
        throw std::runtime_error("DummyJointsDriver::DummyJointsDriver: You must provide a 'joint_group' field");
    }

    joint_group_ = robot.jointGroup(joint_group);
    if (not joint_group_)
    {
        throw std::runtime_error("DummyJointsDriver::DummyJointsDriver: unable to retrieve joint group " + joint_group);
    }
}

bool DummyJointsDriver::init([[maybe_unused]] double timeout)
{
    return true;
}

bool DummyJointsDriver::start()
{
    return true;
}

bool DummyJointsDriver::stop()
{
    return true;
}

bool DummyJointsDriver::read()
{
    std::lock_guard<std::mutex> lock(joint_group_->state_mtx_);
    jointGroupState().position() = joint_group_->command().position();
    jointGroupState().velocity() = joint_group_->command().velocity();
    jointGroupState().acceleration() = joint_group_->command().acceleration();
    jointGroupLastStateUpdate() = std::chrono::high_resolution_clock::now();

    return true;
}

bool DummyJointsDriver::send()
{
    return true;
}

bool DummyJointsDriver::sync()
{
    std::this_thread::sleep_for(std::chrono::milliseconds(size_t(jointGroup()->controlTimeStep() * 1e3)));
    return true;
}

} // namespace rkcl
