/**
 * @file joints_driver.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a generic driver
 * @date 30-01-2020
 * License: CeCILL
 */
#include <rkcl/drivers/joints_driver.h>

#include <utility>

#include <utility>

using namespace rkcl;

JointsDriver::JointsDriver(JointGroupPtr joint_group)
    : joint_group_(std::move(joint_group))
{
}