/**
 * @file dual_arm_force_sensor_driver.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a generic driver
 * @date 30-01-2020
 * License: CeCILL
 */
#include <rkcl/drivers/dual_arm_force_sensor_driver.h>
#include <yaml-cpp/yaml.h>

#include <utility>

#include <utility>

using namespace rkcl;

DualArmForceSensorDriver::DualArmForceSensorDriver(
    ObservationPointPtr op_left_eef,
    ObservationPointPtr op_right_eef)
    : op_left_eef_(std::move(op_left_eef)),
      op_right_eef_(std::move(op_right_eef))
{
}

DualArmForceSensorDriver::DualArmForceSensorDriver(Robot& robot, const YAML::Node& configuration)
{
    if (configuration)
    {
        std::string op_name;
        try
        {
            op_name = configuration["left_end-effector_point_name"].as<std::string>();
        }
        catch (...)
        {
            throw std::runtime_error("DualArmForceSensorDriver::DualArmForceSensorDriver: You must provide 'left_end-effector_point_name' field in the configuration file.");
        }
        op_left_eef_ = robot.observationPoint(op_name);
        if (not op_left_eef_)
        {
            throw std::runtime_error("DualArmForceSensorDriver::DualArmForceSensorDriver: unable to retrieve left_end-effector point name");
        }

        try
        {
            op_name = configuration["right_end-effector_point_name"].as<std::string>();
        }
        catch (...)
        {
            throw std::runtime_error("DualArmForceSensorDriver::DualArmForceSensorDriver: You must provide 'right_end-effector_point_name' field in the configuration file.");
        }
        op_right_eef_ = robot.observationPoint(op_name);
        if (not op_right_eef_)
        {
            throw std::runtime_error("DualArmForceSensorDriver::DualArmForceSensorDriver: unable to retrieve right_end-effector point name");
        }
    }
}
