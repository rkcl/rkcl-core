/**
 * @file internal_functions.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief A set of useful functions for RKCL based on Eigen
 * @date 04-02-2020
 * License: CeCILL
 */
#include <rkcl/processors/internal/internal_functions.h>

#include <iostream>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

Matrix<double, 6, 1> rkcl::internal::computePoseError(const Affine3d& pose1, const Affine3d& pose2)
{
    Matrix<double, 6, 1> error;
    //Position error
    error.head<3>() = pose1.translation() - pose2.translation();

    //Orientation error
    error.tail<3>() = static_cast<Quaterniond>(pose2.linear()).getAngularError(static_cast<Quaterniond>(pose1.linear()));

    return error;
}

Matrix<double, 6, 1> rkcl::internal::absoluteTaskWrench(const Matrix<double, 6, 1>& world_W_larm, const Matrix<double, 6, 1>& world_W_rarm, const Vector3d& larm_p_abs, const Vector3d& rarm_p_abs)
{
    //Absolute forces computed from left and right arm mesured forces
    Matrix<double, 6, 1> world_W_abs;
    world_W_abs.head<3>() = world_W_larm.head<3>() + world_W_rarm.head<3>();
    world_W_abs.tail<3>() = world_W_larm.tail<3>() + world_W_larm.head<3>().skew() * larm_p_abs + world_W_rarm.tail<3>() + world_W_rarm.head<3>().skew() * rarm_p_abs;

    return world_W_abs;
}

Matrix<double, 6, 1> rkcl::internal::considerInteractionPointWrench(const Matrix<double, 6, 1>& W, const Vector3d& p_inter)
{
    Matrix<double, 6, 1> W_inter;
    W_inter.head<3>() = W.head<3>();
    W_inter.tail<3>() = W.tail<3>() - p_inter.cross(W.head<3>());
    return W_inter;
}

Matrix<double, 6, 1> rkcl::internal::relativeTaskWrench(const Matrix<double, 6, 1>& world_W_larm, const Matrix<double, 6, 1>& world_W_rarm)
{
    return (world_W_rarm - world_W_larm) / 2;
}

Matrix<double, 6, 1> rkcl::internal::computePositionControlCmd(const Matrix<double, 6, 6>& S, const Matrix<double, 6, 1>& dx_d, const Matrix<double, 6, 1>& e_x, const Matrix<double, 6, 6>& K)
{
    Matrix<double, 6, 1> dx = S * (dx_d + K * e_x);
    return dx;
}

Matrix<double, 6, 1> rkcl::internal::computeVelocityControlCmd(const Matrix<double, 6, 6>& S, const Matrix<double, 6, 1>& dx_d)
{
    Matrix<double, 6, 1> dx = S * dx_d;
    return dx;
}

Matrix<double, 6, 1> rkcl::internal::computeDampingControlCmd(const Matrix<double, 6, 6>& S, const Matrix<double, 6, 1>& F, const Matrix<double, 6, 6>& B)
{
    Matrix<double, 6, 1> dx = S * B.inverse() * F;
    return dx;
}

Matrix<double, 6, 1> rkcl::internal::computeAdmittanceControlCmd(const Matrix<double, 6, 6>& S, const Matrix<double, 6, 1>& F, const Matrix<double, 6, 6>& K, const Matrix<double, 6, 6>& B, const Matrix<double, 6, 6>& M,
                                                                 const Matrix<double, 6, 1>& e_x, const Matrix<double, 6, 1>& dx_d, const Matrix<double, 6, 1>& e_ddx)
{
    // Matrix<double, 6, 6> m1 = B - (1 / dt) * M;
    // Matrix<double, 6, 1> dx = S * (m1.inverse() * (F + K * e_x + B * dx_d + M * ((1 / dt) * prev_dx + ddx_d)));

    Matrix<double, 6, 1> dx = S * (B.inverse() * (F + K * e_x + M * e_ddx) + dx_d);

    return dx;
}

Matrix<double, 6, 1> rkcl::internal::computeForceControlCmd(const Matrix<double, 6, 6>& S, const Matrix<double, 6, 1>& F, const Matrix<double, 6, 1>& Fd, const Matrix<double, 6, 6>& Kp, const Matrix<double, 6, 6>& Kd,
                                                            double dt, Matrix<double, 6, 1>& prev_e_F)
{
    // double time_constant = 0.1;
    // double filter_coeff = dt/(time_constant+dt);

    Matrix<double, 6, 1> e_F = F - Fd;
    Matrix<double, 6, 1> dx = Kp * e_F;
    // e_F = filter_coeff*e_F + (1.-filter_coeff) *prev_e_F;
    Matrix<double, 6, 1> de_F = (e_F - prev_e_F) / dt;
    dx += Kd * de_F;
    dx = (S * dx).eval();

    prev_e_F = e_F;

    return dx;
}

bool rkcl::internal::computeJointVelocityConstraints(double& XL, double& XU, const double& q_min, const double& q_max, const double& dq_max, const double& ddq_max, const double& q, const double& dq, double dt_joint, double dt_controller)
{
    //See "Motion Control of Redundant Robots under Joint Constraints: Saturation in the Null Space"
    //Note : in the paper, they do not consider the acceleration constraint (-ddq_max*dt+dq)
    // if (dq <= 0)
    // {
    //     if (q <= q_min)
    //         XL = 0;
    //     else
    //     {
    //         //in the worst case, the command will be applied during dt_controller + dt_joint
    //         XL = std::max(-dq_max, (q_min - q) / (dt_controller + dt_joint)); //Position and velocity
    //         XL = std::max(XL, -ddq_max * dt_joint + dq);                      //Acceleration
    //     }
    //     if (dq < 0)
    //         XU = 0;
    // }
    // if (dq >= 0)
    // {
    //     if (q >= q_max)
    //         XU = 0;
    //     else
    //     {
    //         XU = std::min(dq_max, (q_max - q) / (dt_controller + dt_joint)); //Position and velocity
    //         XU = std::min(XU, ddq_max * dt_joint + dq);                      //Acceleration
    //     }
    //     if (dq > 0)
    //         XL = 0;
    // }

    if (q <= q_min)
    {
        XL = 0;
    }
    else
    {
        //in the worst case, the command will be applied during dt_controller + dt_joint
        XL = std::max(-dq_max, (q_min - q) / (dt_controller + dt_joint)); //Position and velocity
        XL = std::max(XL, -ddq_max * dt_joint + dq);                      //Acceleration
        if (dq > 0)
        {
            XL = std::min(XL, 0.);
        }
    }

    if (q >= q_max)
    {
        XU = 0;
    }
    else
    {
        XU = std::min(dq_max, (q_max - q) / (dt_controller + dt_joint)); //Position and velocity
        XU = std::min(XU, ddq_max * dt_joint + dq);                      //Acceleration
        if (dq < 0)
        {
            XU = std::max(XU, 0.);
        }
    }

    return true;
}

bool rkcl::internal::computeJointVelocityConstraintsWithDeceleration(double& XL, double& XU, const double& q_min, const double& q_max, const double& dq_max, const double& ddq_max, const double& q, const double& dq, double dt_joint, double dt_controller)
{
    //See "Motion Control of Redundant Robots under Joint Constraints: Saturation in the Null Space"
    //Note : in the paper, they do not consider the acceleration constraint (-ddq_max*dt+dq)

    //If no acceleration limit has been specified
    if (ddq_max == std::numeric_limits<double>::infinity())
    {
        XL = std::max(-dq_max, (q_min - q) / (dt_controller + dt_joint));
        XU = std::min(dq_max, (q_max - q) / (dt_controller + dt_joint));
    }
    else
    {

        XL = std::max(-dq_max, -ddq_max * dt_joint + dq);
        // double q_next_min = q + dq*(dt_controller + dt_joint) - 0.5 * ddq_max * (dt_controller + dt_joint) * (dt_controller + dt_joint);
        double q_next_min = q + XL * (dt_controller + dt_joint);
        XL = std::max(XL, -sqrt(2 * ddq_max * (q_next_min - q_min)));
        // XL = std::max(XL, (q_min - q)/dt);
        XU = std::min(dq_max, ddq_max * dt_joint + dq);
        // double q_next_max = q + dq*(dt_controller + dt_joint) + 0.5 * ddq_max * (dt_controller + dt_joint) * (dt_controller + dt_joint);
        double q_next_max = q + XU * (dt_controller + dt_joint);
        XU = std::min(XU, sqrt(2 * ddq_max * (q_max - q_next_max)));
        // XU = std::min(XU, (q_max - q)/dt);

        //In some cases, XL > XU because of the acceleration limit try to fix this
        if (XL > XU)
        {
            if (XL == (-sqrt(2 * ddq_max * (q_next_min - q_min))))
            {
                XL = XU - 1e-10;
            }
            else
            {
                XU = XL + 1e-10;
            }
            return false;
        }
    }
    return true;
}

bool rkcl::internal::computeTaskVelocityConstraints(double& XL, double& XU, const double& dx_max, const double& ddx_max, const double& dx, double dt)
{
    // Without deceleration constraints
    XU = 0;
    XL = 0;
    if (dx <= 0)
    {
        XL = std::max(-dx_max, -ddx_max * dt + dx); //Velocity and Acceleration
        XU = std::max(0., ddx_max * dt + dx);
    }
    if (dx >= 0)
    {
        XL = std::min(0., -ddx_max * dt + dx);
        XU = std::min(dx_max, ddx_max * dt + dx); //Velocity and Acceleration
    }

    // With deceleration constraints
    // XL = std::max(-dx_max, -ddx_max * dt + dx);
    // XU = std::min(dx_max, ddx_max * dt + dx);

    return true;
}

VectorXd rkcl::internal::computeJointLimitsRepulsiveVec(const VectorXd& q, const VectorXd& q_min, const VectorXd& q_max, const VectorXd& dq_min, const VectorXd& dq_max, double beta)
{
    //Define a second task to steer joint away from their limits
    int nb_joints = q.rows();
    VectorXd q_mean = (q_max - q_min) / 2;
    VectorXd qt_min = q_min + beta * q_mean;
    VectorXd qt_max = q_max - beta * q_mean;

    VectorXd repulsive_vec = VectorXd::Zero(nb_joints);

    for (int i = 0; i < nb_joints; ++i)
    {
        if (q(i) < qt_min(i))
        {
            repulsive_vec(i) = (pow(qt_min(i) - q(i), 2) / pow(qt_min(i) - q_min(i), 2)) * dq_max(i);
        }
        else if (q(i) > qt_max(i))
        {
            repulsive_vec(i) = (pow(q(i) - qt_max(i), 2) / pow(q_max(i) - qt_max(i), 2)) * dq_min(i);
        }
        else
        {
            repulsive_vec(i) = 0;
        }
    }

    return repulsive_vec;
}
