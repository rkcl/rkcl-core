/**
 * @file joints_controller.cpp
 * @author Sonny Tarbouriech (LIRMM), Benjamin Navarro (LIRMM)
 * @brief Define a joint controller
 * @date 24-01-2020
 * License: CeCILL
 */
#include <rkcl/processors/simple_joints_controller.h>

#include <yaml-cpp/yaml.h>

#include <iostream>
#include <utility>

using namespace rkcl;

SimpleJointsController::SimpleJointsController(
    JointGroupPtr joint_group)
    : JointsController(std::move(joint_group))
{
}

SimpleJointsController::SimpleJointsController(const JointGroupPtr& joint_group, const YAML::Node& configuration)
    : JointsController(std::move(joint_group))
{
    if (configuration)
    {
        auto& proportional_gain = configuration["proportional_gain"];
        if (proportional_gain)
        {
            proportional_gain_ = proportional_gain.as<double>(1);
        }
    }
}

void SimpleJointsController::reset()
{
    prev_target_position_ = joint_group_->state().position();
}

void SimpleJointsController::computeJointVelocityCommand()
{
    if (joint_group_->enableJointSpaceErrorCompensation())
        jointGroupCommand().velocity() = joint_group_->selectionMatrix().value() * (joint_group_->target().velocity() + proportional_gain_ * (prev_target_position_ - joint_group_->state().position()));
    else
        jointGroupCommand().velocity() = joint_group_->selectionMatrix().value() * joint_group_->target().velocity();
}

bool SimpleJointsController::process()
{
    std::lock_guard<std::mutex> lock(joint_group_->command_mtx_);
    // if (joint_group_->controlSpace() == JointGroup::ControlSpace::JointSpace)
    // {
    //     jointGroupCommand().velocity() = joint_group_->target().velocity();
    //     jointGroupCommand().position() = joint_group_->target().position();
    // }
    // else if (joint_group_->controlSpace() == JointGroup::ControlSpace::TaskSpace)
    // {
    //     // jointGroupCommand().position() = joint_group_->state().position() + joint_group_->command().velocity() * joint_group_->controlTimeStep();
    //     jointGroupCommand().position() = joint_group_->command().position() + joint_group_->command().velocity() * joint_group_->controlTimeStep();
    //     jointGroupCommand().acceleration() = (joint_group_->command().velocity() - joint_group_->state().velocity()) / joint_group_->controlTimeStep();
    // }

    if (joint_group_->controlSpace() == JointGroup::ControlSpace::JointSpace)
    {
        computeJointVelocityCommand();
    }
    jointGroupCommand().position() = joint_group_->command().position() + joint_group_->command().velocity() * joint_group_->controlTimeStep();
    jointGroupCommand().acceleration() = (joint_group_->command().velocity() - joint_group_->state().velocity()) / joint_group_->controlTimeStep();

    prev_target_position_ = joint_group_->target().position();

    return true;
}
