/**
 * @file cooperative_task_adapter.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Specific to dual-arm robots : transformation from individual arm representation to the cooperative task representation
 * @date 30-01-2020
 * License: CeCILL
 */
#include <rkcl/processors/cooperative_task_adapter.h>
#include <rkcl/processors/internal/internal_functions.h>

#include <yaml-cpp/yaml.h>

#include <utility>

#include <utility>

using namespace rkcl;

CooperativeTaskAdapter::CooperativeTaskAdapter(ObservationPointPtr left_end_effector_op,
                                               ObservationPointPtr right_end_effector_op,
                                               ObservationPointPtr absolute_task_op,
                                               ObservationPointPtr relative_task_op)
    : left_end_effector_op_(std::move(left_end_effector_op)),
      right_end_effector_op_(std::move(right_end_effector_op)),
      absolute_task_op_(std::move(absolute_task_op)),
      relative_task_op_(std::move(relative_task_op))
{
}

CooperativeTaskAdapter::CooperativeTaskAdapter(
    Robot& robot,
    const YAML::Node& configuration)
{
    if (configuration)
    {
        std::string op_name;
        try
        {
            op_name = configuration["left_end-effector_point_name"].as<std::string>();
        }
        catch (...)
        {
            throw std::runtime_error("CooperativeTaskAdapter::CooperativeTaskAdapter: You must provide 'left_end-effector_point_name' field in the configuration file.");
        }
        left_end_effector_op_ = robot.observationPoint(op_name);
        if (not leftEndEffectorOP())
        {
            throw std::runtime_error("CooperativeTaskAdapter::CooperativeTaskAdapter: unable to retrieve left_end-effector point name");
        }

        try
        {
            op_name = configuration["right_end-effector_point_name"].as<std::string>();
        }
        catch (...)
        {
            throw std::runtime_error("CooperativeTaskAdapter::CooperativeTaskAdapter: You must provide 'right_end-effector_point_name' field in the configuration file.");
        }
        right_end_effector_op_ = robot.observationPoint(op_name);
        if (not rightEndEffectorOP())
        {
            throw std::runtime_error("CooperativeTaskAdapter::CooperativeTaskAdapter: unable to retrieve right_end-effector point name");
        }

        try
        {
            op_name = configuration["absolute_task_point_name"].as<std::string>();
        }
        catch (...)
        {
            throw std::runtime_error("CooperativeTaskAdapter::CooperativeTaskAdapter: You must provide 'absolute_task_point_name' field in the configuration file.");
        }
        absolute_task_op_ = robot.observationPoint(op_name);
        if (not absoluteTaskOP())
        {
            throw std::runtime_error("CooperativeTaskAdapter::CooperativeTaskAdapter: unable to retrieve absolute_task point name");
        }

        try
        {
            op_name = configuration["relative_task_point_name"].as<std::string>();
        }
        catch (...)
        {
            throw std::runtime_error("CooperativeTaskAdapter::CooperativeTaskAdapter: You must provide 'relative_task_point_name' field in the configuration file.");
        }
        relative_task_op_ = robot.observationPoint(op_name);
        if (not relativeTaskOP())
        {
            throw std::runtime_error("CooperativeTaskAdapter::CooperativeTaskAdapter: unable to retrieve relative_task point name");
        }
    }
    else
    {
        throw std::runtime_error("CooperativeTaskAdapter::CooperativeTaskAdapter: missing 'cooperative_task_adapter' configuration");
    }
}

void CooperativeTaskAdapter::computeAbsoluteTaskWrench()
{
    Eigen::Vector3d left_eef_pos_w = leftEndEffectorOP()->state().pose().translation();
    auto left_eef_wrench_w = leftEndEffectorOP()->state().wrench();
    if (leftEndEffectorOP()->refBodyName() != "world")
    {
        left_eef_pos_w = (leftEndEffectorOP()->refBodyPoseWorld().linear() * left_eef_pos_w).eval();
        left_eef_wrench_w.head<3>() = (leftEndEffectorOP()->refBodyPoseWorld().linear() * left_eef_wrench_w.head<3>()).eval();
        left_eef_wrench_w.tail<3>() = (leftEndEffectorOP()->refBodyPoseWorld().linear() * left_eef_wrench_w.tail<3>()).eval();
    }

    Eigen::Vector3d right_eef_pos_w = rightEndEffectorOP()->state().pose().translation();
    auto right_eef_wrench_w = rightEndEffectorOP()->state().wrench();
    if (rightEndEffectorOP()->refBodyName() != "world")
    {
        right_eef_pos_w = (rightEndEffectorOP()->refBodyPoseWorld().linear() * right_eef_pos_w).eval();
        right_eef_wrench_w.head<3>() = (rightEndEffectorOP()->refBodyPoseWorld().linear() * right_eef_wrench_w.head<3>()).eval();
        right_eef_wrench_w.tail<3>() = (rightEndEffectorOP()->refBodyPoseWorld().linear() * right_eef_wrench_w.tail<3>()).eval();
    }

    Eigen::Vector3d absolute_task_pos_w = absoluteTaskOP()->state().pose().translation();
    if (absoluteTaskOP()->refBodyName() != "world")
    {
        absolute_task_pos_w = (absoluteTaskOP()->refBodyPoseWorld().linear() * absolute_task_pos_w + absoluteTaskOP()->refBodyPoseWorld().translation()).eval();
    }

    Eigen::Vector3d larm_p_abs = absolute_task_pos_w - left_eef_pos_w;
    Eigen::Vector3d rarm_p_abs = absolute_task_pos_w - right_eef_pos_w;

    absolute_task_op_->_state().wrench() = rkcl::internal::absoluteTaskWrench(left_eef_wrench_w,
                                                                              right_eef_wrench_w,
                                                                              larm_p_abs,
                                                                              rarm_p_abs);

    if (absoluteTaskOP()->refBodyName() != "world")
    {
        absolute_task_op_->_state().wrench().head<3>() = absoluteTaskOP()->refBodyPoseWorld().linear().transpose() * absolute_task_op_->_state().wrench().head<3>();
        absolute_task_op_->_state().wrench().tail<3>() = absoluteTaskOP()->refBodyPoseWorld().linear().transpose() * absolute_task_op_->_state().wrench().tail<3>();
    }
}

void CooperativeTaskAdapter::computeRelativeTaskWrench()
{
    auto left_eef_wrench_w = leftEndEffectorOP()->state().wrench();
    if (leftEndEffectorOP()->refBodyName() != "world")
    {
        left_eef_wrench_w.head<3>() = (leftEndEffectorOP()->refBodyPoseWorld().linear() * left_eef_wrench_w.head<3>()).eval();
        left_eef_wrench_w.tail<3>() = (leftEndEffectorOP()->refBodyPoseWorld().linear() * left_eef_wrench_w.tail<3>()).eval();
    }

    auto right_eef_wrench_w = rightEndEffectorOP()->state().wrench();
    if (rightEndEffectorOP()->refBodyName() != "world")
    {
        right_eef_wrench_w.head<3>() = (rightEndEffectorOP()->refBodyPoseWorld().linear() * right_eef_wrench_w.head<3>()).eval();
        right_eef_wrench_w.tail<3>() = (rightEndEffectorOP()->refBodyPoseWorld().linear() * right_eef_wrench_w.tail<3>()).eval();
    }

    relative_task_op_->_state().wrench() = rkcl::internal::relativeTaskWrench(left_eef_wrench_w,
                                                                              right_eef_wrench_w);

    if (relativeTaskOP()->refBodyName() != "world")
    {
        relative_task_op_->_state().wrench().head<3>() = relativeTaskOP()->refBodyPoseWorld().linear().transpose() * relative_task_op_->_state().wrench().head<3>();
        relative_task_op_->_state().wrench().tail<3>() = relativeTaskOP()->refBodyPoseWorld().linear().transpose() * relative_task_op_->_state().wrench().tail<3>();
    }
}

bool CooperativeTaskAdapter::process()
{
    computeRelativeTaskWrench();
    computeAbsoluteTaskWrench();
    return true;
}
