/**
 * @file simple_collision_avoidance.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a simple collision avoidance strategy using spheres
 * @date 30-01-2020
 * License: CeCILL
 */
#include <rkcl/processors/simple_collision_avoidance.h>
#include <yaml-cpp/yaml.h>
#include <iostream>
#include <utility>

using namespace rkcl;

SimpleCollisionAvoidance::SimpleCollisionAvoidance(Robot& robot, ForwardKinematicsPtr fk)
    : CollisionAvoidance(robot, std::move(fk))
{
}

SimpleCollisionAvoidance::SimpleCollisionAvoidance(Robot& robot, ForwardKinematicsPtr fk, const YAML::Node& configuration)
    : CollisionAvoidance(robot, std::move(fk), configuration)
{
}

void SimpleCollisionAvoidance::init()
{
    //Create collision objects from robot's description file parsed in fk class
    forward_kinematics_->createRobotCollisionObjects(robot_collision_objects_);

    createCollisionPairs();
}

void SimpleCollisionAvoidance::reset()
{
}

void SimpleCollisionAvoidance::createCollisionPairs()
{
    //Create pairs
    collision_pairs_.clear();
    for (size_t i = 0; i < robotCollisionObjects().size(); ++i)
    {
        if (std::holds_alternative<geometry::Sphere>(robotCollisionObject(i)->geometry()))
        {
            for (size_t j = i + 1; j < robot_collision_objects_.size(); ++j)
            {
                if (std::holds_alternative<geometry::Sphere>(robotCollisionObject(j)->geometry()))
                {
                    bool collision_disabled = (robotCollisionObject(i)->linkName() == robotCollisionObject(j)->linkName());
                    collision_disabled |= std::find(robotCollisionObject(i)->disableCollisionLinkName().begin(), robotCollisionObject(i)->disableCollisionLinkName().end(), robotCollisionObject(j)->linkName()) != robotCollisionObject(i)->disableCollisionLinkName().end();
                    collision_disabled |= std::find(robotCollisionObject(j)->disableCollisionLinkName().begin(), robotCollisionObject(j)->disableCollisionLinkName().end(), robotCollisionObject(i)->linkName()) != robotCollisionObject(j)->disableCollisionLinkName().end();
                    if (not collision_disabled)
                    {
                        CollisionPair collision_pair;
                        collision_pair.collision_object1_index = i;
                        collision_pair.collision_object2_index = j;

                        collision_pairs_.push_back(collision_pair);
                    }
                }
            }
        }
        else
        {
            std::cerr << "SimpleCollisionAvoidance::init WARNING: only Sphere geometry is supported, object attached to '" << robotCollisionObject(i)->linkName() << "' has been ignored" << '\n';
        }
    }
}

bool SimpleCollisionAvoidance::computeWitnessPoints()
{
    for (const auto& collision_pair : collision_pairs_)
    {
        auto p1_center = robotCollisionObject(collision_pair.collision_object1_index)->poseWorld().translation();
        auto p2_center = robotCollisionObject(collision_pair.collision_object2_index)->poseWorld().translation();
        auto v_norm = (p2_center - p1_center).normalized();

        const auto& sphere1_data = std::get<geometry::Sphere>(robotCollisionObject(collision_pair.collision_object1_index)->geometry());
        const auto& sphere2_data = std::get<geometry::Sphere>(robotCollisionObject(collision_pair.collision_object2_index)->geometry());

        auto p1_wit = p1_center + sphere1_data.radius().value() * v_norm;
        auto p2_wit = p2_center - sphere2_data.radius().value() * v_norm;

        if ((p2_wit - p1_center).norm() <= sphere1_data.radius())
        {
            std::cerr << "SimpleCollisionAvoidance: collision detected between '" << robotCollisionObject(collision_pair.collision_object1_index)->linkName()
                      << "' and '" << robotCollisionObject(collision_pair.collision_object2_index)->linkName() << std::endl;
            return false;
        }
        else if ((p2_wit - p1_wit).norm() < collisionPreventParameters().activationDistance())
        {
            RobotCollisionObject::SelfCollisionEval self_collision_eval;
            self_collision_eval.otherLinkName() = robotCollisionObject(collision_pair.collision_object2_index)->linkName();
            self_collision_eval.witnessPoint() = p1_wit;
            self_collision_eval.otherWitnessPoint() = p2_wit;

            robotCollisionObjectSelfCollisionPreventEvals(collision_pair.collision_object1_index).push_back(self_collision_eval);
        }
    }
    return true;
}

double SimpleCollisionAvoidance::computeVelocityDamper(const Eigen::Vector3d& point1, const Eigen::Vector3d& point2)
{
    double velocity_damper;

    if ((point2 - point1).norm() > collisionPreventParameters().limitDistance())
    {
        velocity_damper = collisionPreventParameters().damperFactor() * (((point2 - point1).norm() - collisionPreventParameters().limitDistance()) / (collisionPreventParameters().activationDistance() - collisionPreventParameters().limitDistance()));
    }
    else
    {
        velocity_damper = 0;
    }
    return velocity_damper;
}

void SimpleCollisionAvoidance::setRobotVelocityDamper()
{
    size_t constraints_count = 0;
    for (const auto& rco : robot_collision_objects_)
    {
        constraints_count += rco->selfCollisionPreventEvals().size();
    }

    for (size_t i = 0; i < robot_.jointGroupCount(); ++i)
    {
        jointGroupInternalConstraints(i).vectorInequality().resize(constraints_count);
    }

    size_t constraint_index = 0;
    for (const auto& rco : robot_collision_objects_)
    {
        for (const auto& self_collision_eval : rco->selfCollisionPreventEvals())
        {
            auto velocity_damper = computeVelocityDamper(self_collision_eval.witnessPoint(), self_collision_eval.otherWitnessPoint());
            for (size_t i = 0; i < robot_.jointGroupCount(); ++i)
            {
                jointGroupInternalConstraints(i).vectorInequality()(constraint_index) = velocity_damper;
            }
            ++constraint_index;
        }
    }
}

void SimpleCollisionAvoidance::clearData()
{
    //Delete old witness points
    for (auto i = 0u; i < robot_collision_objects_.size(); ++i)
    {
        robotCollisionObjectSelfCollisionPreventEvals(i).clear();
    }
}

std::vector<Eigen::Vector3d> SimpleCollisionAvoidance::getWitnessPoints()
{
    std::vector<Eigen::Vector3d> points;

    for (auto i = 0u; i < robot_collision_objects_.size(); ++i)
    {
        for (const auto& self_collision_eval : robotCollisionObjectSelfCollisionPreventEvals(i))
        {
            points.push_back(self_collision_eval.witnessPoint());
            points.push_back(self_collision_eval.otherWitnessPoint());
        }
    }

    return points;
}

bool SimpleCollisionAvoidance::process()
{
    forward_kinematics_->updateRobotCollisionObjectsPose(robot_collision_objects_);

    clearData();
    bool collision_free = computeWitnessPoints();
    setRobotVelocityDamper();

    forward_kinematics_->computeCollisionAvoidanceConstraints(robot_collision_objects_);

    return collision_free;
}
