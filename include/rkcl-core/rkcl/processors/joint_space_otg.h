/**
 * @file task_space_otg.h
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a generic joint space online trajectory generator
 * @date 15-05-2020
 * License: CeCILL
 */

#pragma once

#include <rkcl/processors/otg.h>

#include <memory>

/**
  * @brief  Namespace for everything related to RKCL
  */
namespace rkcl
{

/**
 * @brief A class for generating task space trajectories using Reflexxes
 */
class JointSpaceOTG : public OnlineTrajectoryGenerator
{
public:
    /**
	 * @brief Enum class used to define whether the otg is position-based or velocity-based
	 *
	 */
    enum class ControlMode
    {
        Position,
        Velocity
    };

    /**
	* @brief Construct a new Joint Space OTG
	* @param joint_group a pointer to the joint group to consider in the trajectory generation
	*/
    explicit JointSpaceOTG(JointGroupPtr joint_group);

    /**
	* @brief Construct a new Joint Space OTG from a YAML configuration file
    * Accepted values are : 'input_data'
	* @param robot a reference to the shared robot holding the control points
	* @param configuration a YAML node containing the configuration of the driver
	*/
    JointSpaceOTG(
        Robot& robot,
        const YAML::Node& configuration);

    /**
     * @brief Destroy the Joint Space OTG
     */
    ~JointSpaceOTG() override;

    /**
	 * @brief Getter to the joint group object
	 * @return Pointer to the joint group
	 */
    const auto& jointGroup() const;

    /**
     * @brief Get the otg control mode
     * @return the control mode
     */
    const auto& controlMode() const;

    const auto& previousStatePosition() const;

    /**
     * @brief Set the otg control mode
     * @return the control mode
     */
    auto& controlMode();

    /**
	 * @brief Configure the trajectory generator using a YAML configuration file
     * Accepted values are : 'input_data'
	 * @param robot a reference to the shared robot
	 * @param configuration a YAML node containing the configuration of the trajectory generator
	 */
    void configure(Robot& robot, const YAML::Node& configuration);

    /**
     * @brief Re-configure the trajectory generator using a YAML configuration file
     * @param configuration a YAML node containing the configuration of the trajectory generator
     */
    void reConfigure(const YAML::Node& configuration);

    virtual void init();
    void reset() override;

protected:
    JointGroupPtr joint_group_; //!< Pointer to the joint group to consider in the trajectory generation
    ControlMode control_mode_;  //!< define whether the otg is position-based or velocity-based

    Eigen::VectorXd previous_state_position_;
};
inline const auto& JointSpaceOTG::jointGroup() const
{
    return joint_group_;
}
// inline auto& JointSpaceOTG::_jointGroup()
// {
//     return joint_group_;
// }
inline const auto& JointSpaceOTG::controlMode() const
{
    return control_mode_;
}
inline auto& JointSpaceOTG::controlMode()
{
    return control_mode_;
}
inline const auto& JointSpaceOTG::previousStatePosition() const
{
    return previous_state_position_;
}

using JointSpaceOTGPtr = std::shared_ptr<JointSpaceOTG>;
using JointSpaceOTGConstPtr = std::shared_ptr<const JointSpaceOTG>;

/**
 * Check that the joint space otg handles the joint group with the given name
 * @param joint_space_otg joint space otg to check
 * @param joint_group_name expected joint group name
 * @return true if the joint space otg handles the joint group with the given name, false otherwise
 */
bool operator==(const JointSpaceOTG& joint_space_otg, const std::string& joint_group_name);

/**
 * Check that the pointed joint space otg handles the joint group with the given name
 * @param joint_space_otg pointer to the joint space otg to check
 * @param joint_group_name expected joint group name
 * @return true if the pointed joint space otg handles the joint group with the given name, false otherwise
 */
bool operator==(const JointSpaceOTGPtr& joint_space_otg, const std::string& joint_group_name);

} // namespace rkcl
