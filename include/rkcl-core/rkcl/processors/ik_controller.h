/**
 * @file ik_controller.h
 * @author Sonny Tarbouriech (LIRMM), Benjamin Navarro (LIRMM)
 * @brief Define an inverse kinematic controller
 * @date 28-01-2020
 * License: CeCILL
 */

#pragma once

#include <rkcl/data/fwd_decl.h>
#include <rkcl/data/crtp.h>
#include <rkcl/data/robot.h>

namespace rkcl
{

/**
 * @brief Class which solves the IK problem using different QP-based approaches
 */
class InverseKinematicsController : public Callable<InverseKinematicsController>
{
public:
    /**
     * @brief Construct a new Inverse Kinematics Controller object
     *
     * @param robot a reference to the shared robot object
     */
    InverseKinematicsController(
        Robot& robot);

    /**
	 * Default destructor.
	 */
    virtual ~InverseKinematicsController() = default;

    const auto& robot() const;

    /**
     * @brief Initialize the ik controller (called once at the beginning of the program)
     */
    virtual void init() = 0;
    /**
     * @brief Reset the ik controller (called at the beginning of each new task)
     */
    virtual void reset() = 0;

    void resetInternalCommand();

    /**
	 * @brief Set the IK parameters according to the given YAML configuration node.
	 * @param configuration YAML node containing all the parameters for IK configuration
	 * @return True if OK, false otherwise
	 */
    virtual bool configure(const YAML::Node& configuration) = 0;

    /**
     * @brief Estimate the current joint state position based on their last update and their actual velocity
     * @param joint_group_index index of the joint group
     */
    void updateJointState(size_t joint_group_index);

    /**
	 * @brief Run the controller to accomplish the current tasks. Will generate commands for joint velocities.
	 * @return true if a valid solution is found, false otherwise.
	 */
    virtual bool process() = 0;

protected:
    Robot& robot_; //!< Reference to the shared robot object

    auto& jointGroupConstraints(const size_t& joint_group_index);
    auto& jointGroupInternalCommand(const size_t& joint_group_index);

    auto& controlPointState(const size_t& control_point_index);
};

inline const auto& InverseKinematicsController::robot() const
{
    return robot_;
}
inline auto& InverseKinematicsController::jointGroupConstraints(const size_t& joint_group_index)
{
    return robot_.jointGroup(joint_group_index)->_constraints();
}
inline auto& InverseKinematicsController::jointGroupInternalCommand(const size_t& joint_group_index)
{
    return robot_.jointGroup(joint_group_index)->_internalCommand();
}

inline auto& InverseKinematicsController::controlPointState(const size_t& control_point_index)
{
    return robot_.controlPoint(control_point_index)->_state();
}

/**
 * @brief Factory class allowing to gather the different QP solvers
 *
 */
class IKControllerFactory
{
public:
    using create_method_t = std::function<std::shared_ptr<InverseKinematicsController>(Robot&, const YAML::Node&)>;

    ~IKControllerFactory() = default;

    /**
	 * @brief Add a new QP solver to the factory based on a given creation function
	 * @param  name          name of the QP solver
	 * @param  create_method function used to create the QP solver
	 * @return               true on success, false otherwise
	 */
    static bool add(const std::string& name, create_method_t create_method)
    {
        auto it = createMethods().find(name);
        if (it == createMethods().end())
        {
            createMethods()[name] = std::move(create_method);
            return true;
        }
        return false;
    }

    /**
	 * @brief Add a new QP solver to the factory. Expect a constructor that takes (Robot&, const YAML::Node&)
	 * @param  name          name of the QP solver
	 * @return               true on success, false otherwise
	 */
    template <typename T>
    static bool add(const std::string& name)
    {
        auto it = createMethods().find(name);
        if (it == createMethods().end())
        {
            createMethods()[name] =
                [](Robot& robot, const YAML::Node& conf) -> std::shared_ptr<InverseKinematicsController> {
                return std::make_shared<T>(robot, conf);
            };
            return true;
        }
        return false;
    }

    /**
	 * @brief Instantiate a new QP solver
	 * @param  name          name of the QP solver
	 * @param  configuration configuration passed to the QP solver
	 * @return               a shared_ptr to the QP solver (can be null if the QP solver has not been registered in the factory)
	 */
    static std::shared_ptr<InverseKinematicsController> create(const std::string& name, Robot& robot, const YAML::Node& configuration)
    {
        auto it = createMethods().find(name);
        if (it != createMethods().end())
        {
            return it->second(robot, configuration);
        }

        return nullptr;
    }

private:
    /**
	 * @brief Construct a new IKControllerFactory object
	 *
	 */
    IKControllerFactory() = default;

    static std::map<std::string, create_method_t>& createMethods()
    {
        static std::map<std::string, create_method_t> create_methods;
        return create_methods;
    }
};

using InverseKinematicsControllerPtr = std::shared_ptr<InverseKinematicsController>;
} // namespace rkcl
