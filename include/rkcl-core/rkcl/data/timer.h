/**
 * @file timer.h
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a timer (compatible with simulation)
 * @date 24-01-2020
 * License: CeCILL
 */

#pragma once

#include <thread>

namespace rkcl
{

/**
 * @brief Class defining a timer to manage sleep duration in threads.
 * It integrates a real time factor for simulation environments.
 */
class Timer
{
public:
    /**
	 * @brief Construct a new Timer object
	 *
	 */
    Timer();

    /**
	 * @brief Destroy the Timer object
	 *
	 */
    ~Timer() = default;

    /**
	 * @brief Sleep for a given duration from now on
	 * @param sleep_duration duration of the sleep
	 */
    static void sleepFor(const std::chrono::microseconds& sleep_duration);
    /**
	 * @brief Sleep for a given duration from a time point in the past
	 * @param reference_time Time point reference
	 * @param sleep_duration duration of the sleep
	 */
    static void sleepFor(const std::chrono::high_resolution_clock::time_point& reference_time, const std::chrono::microseconds& sleep_duration);

    /**
	 * @brief Accessor function to get the real time factor
	 * @return value of the real time factor
	 */
    static double& realTimeFactor();

private:
    static double real_time_factor_; //!< Static double in ]0,1] which indicates the factor between real and simulation times
};

inline double& Timer::realTimeFactor()
{
    return real_time_factor_;
}

} // namespace rkcl
