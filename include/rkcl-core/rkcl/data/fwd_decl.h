/**
 * @file fwd_decl.h
 * @author Sonny Tarbouriech (LIRMM), Benjamin Navarro (LIRMM)
 * @brief Forward declaration of commonly used classes
 * @date 28-01-2020
 * License: CeCILL
 */
#pragma once

namespace YAML
{
class Node;
}

namespace rkcl
{
class InverseKinematicsController;
class JointController;
class TaskSpaceController;
class WrenchController;
} // namespace rkcl
