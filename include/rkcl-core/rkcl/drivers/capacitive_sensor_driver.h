/**
 * @file capacitive_sensor_driver.h
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a generic driver to read the state of capacitive sensors
 * @date 20-02-2020
 * License: CeCILL
 */

#pragma once

#include <rkcl/drivers/driver.h>
#include <rkcl/processors/collision_avoidance.h>
#include <string>
#include <vector>

/**
 * @brief Namespace for everything related to RKCL
 */
namespace rkcl
{

class CapacitiveSensorDriver : public Driver
{
public:
    explicit CapacitiveSensorDriver(CollisionAvoidancePtr collision_avoidance);

protected:
    CollisionAvoidancePtr collision_avoidance_; //!< A pointer to the collision avoidance object

    auto robotCollisionObject(const std::string& rco_name);
    auto robotCollisionObject(size_t rco_index);
};

inline auto CapacitiveSensorDriver::robotCollisionObject(const std::string& rco_name)
{
    return collision_avoidance_->_robotCollisionObject(rco_name);
}

inline auto CapacitiveSensorDriver::robotCollisionObject(size_t rco_index)
{
    return collision_avoidance_->_robotCollisionObject(rco_index);
}
using CapacitiveSensorDriverPtr = std::shared_ptr<CapacitiveSensorDriver>;
using CapacitiveSensorDriverConstPtr = std::shared_ptr<const CapacitiveSensorDriver>;

} // namespace rkcl
